let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let userSchema = new Schema({
    id: { type: String },
    name: { type: String },
    age: { type: Number, min: 0 }
}, { versionKey: false });

let User = mongoose.model('Users', userSchema);
module.exports = User;