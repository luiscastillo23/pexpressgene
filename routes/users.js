let express = require('express');
let router = express.Router();
let conexion = require('../config/conexion');

let User = require('../models/user');

/* GET users listing. */
router.get('/', (req, res, next) => {
  User.find((err, users) => {
    console.log(users);
    if (err) throw err;
    res.render('users', { title: 'Express', users: users });
  });
 // res.send('respond with a resource');
});

/* New User */
router.get('/new', (req, res, next) => {
  res.render('userForm', {});
});

router.post('/operate', (req, res, next) => {
  console.log(req.body);  

  if (req.body._id === "") {
    let user = new User({
      name: req.body.name,
      age: req.body.age
    });
    
    user.save();
  } else {    
    //console.log(req.body._id);
    User.findByIdAndUpdate(req.body._id, { $set: req.body }, { new: true }, (err, model) => {
      if (err) throw err;
    });
  }
  res.redirect('/users');
});

router.get('/edit/:id', (req, res, next) => {
  let idUser = req.params.id;  
  User.findOne({_id: idUser }, (err, user) => {
    console.log(user);
    if (err) throw err;
    res.render('userForm', { user: user });
  });
});

router.get('/delete/:id', (req, res, next) => {
  let idUser = req.params.id;

  User.remove({_id: idUser }, (err) => {
    if (err) throw err;
    res.redirect('/users');
  });
});

module.exports = router;
